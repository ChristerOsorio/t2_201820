package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	public void add(T elemento);
	
	public void addAtEnd (T elemento);
	
	public void addAtK(T elemento, int posicion) throws Exception;
	
	public T getElement (int posicion) throws Exception;
	
//	public T getCurrentElement();
//	
//	public void delete ();
	
	public void deleteAtK(int posicion) throws Exception;

}
