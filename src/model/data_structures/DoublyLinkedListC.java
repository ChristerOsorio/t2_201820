package model.data_structures;

import java.util.Iterator;

/*
 * Basada en mi ejercicio de nivel 9 de apo 2, el libro de introduccion a las estructuras de datos de Villalobos, y 
 * de la implementacion de Pablo Barvo para Cupi2 
 * (recuperado de:https://cupintranet.virtual.uniandes.edu.co/sitio/index.php/cursos/estructuras-de-datos/cupi2collections
 * /estructuras-de-datos/lista-encadenada/visualizacion-codigo-fuente/listaencadenada)
 */


public class DoublyLinkedListC <T> implements DoublyLinkedList<T>, Iterable <T> {

	//Atributos
	
	private Nodo <T> primero;
	private Nodo <T>ultimo;
	private int numeroElementosLista;

	//Constructor
	public DoublyLinkedListC() {
		primero = null;
		ultimo = null;
		numeroElementosLista=0;
	}


	//Metodos



	@Override
	public Integer getSize() {

		return numeroElementosLista;
	}

	@Override
	public void add(T pItem) {
		// 
		Nodo <T> nodoAgregar= new Nodo<T>(pItem);

		if (primero==null)
		{
			primero= nodoAgregar;
			ultimo=nodoAgregar;

		}

		else
		{

			primero.insertarNodoAntesDelActual(nodoAgregar);
			primero=nodoAgregar;
		}

		numeroElementosLista++;
	}

	@Override
	public void addAtEnd(T pItem) {
		Nodo <T> nodoAgregar= new Nodo<T>(pItem);

		if (primero==null)
		{
			primero= nodoAgregar;
			ultimo=nodoAgregar;

		}

		else
		{

			ultimo.insertarNodoDespuesDelActual(nodoAgregar);
			ultimo=nodoAgregar;
		}

		numeroElementosLista++;
	}

	@Override
	public void addAtK(T pItem, int pPosicion) throws Exception {


		if (pPosicion>=numeroElementosLista)
		{
			throw new Exception("posición no valida en la lista");
		}

		Nodo<T> nodoAgregar= new Nodo <T>(pItem);

		Nodo<T> nodoActual= primero;

		for(int i=0; i<pPosicion-1; i++)
		{
			nodoActual=nodoActual.darSiguienteNodo();
		}

		nodoActual.insertarNodoDespuesDelActual(nodoAgregar);
		numeroElementosLista++;



	}

	@Override
	public T getElement(int pPosicion) throws Exception{
		if (pPosicion>=numeroElementosLista)
		{
			throw new Exception("posición no valida en la lista");
		}

		

		Nodo<T> nodoActual= primero;

		for(int i=0; i<pPosicion-1; i++)
		{
			nodoActual=nodoActual.darSiguienteNodo();
		}

		return nodoActual.darItem();
	}

//	@Override
//	public T getCurrentElement() {
//		return primero.darItem();
//	}
//
//	@Override
//	public void delete() {
//		// TODO Auto-generated method stub
//
//	}

	@Override
	public void deleteAtK(int pPosicion) throws Exception  {
		
		if(  pPosicion >= numeroElementosLista )
		{
		throw new Exception();
		}
		else if( pPosicion == 0 )
		{
		if( primero.equals( ultimo ) )
		{
		ultimo = null;
		}
		
		primero = primero.desconectarPrimero( );
		numeroElementosLista--;
		
		}
		else
		{

		Nodo<T> p = primero.darSiguienteNodo( );
		for( int cont = 1; cont < pPosicion; cont++ )
		{
		p = p.darSiguienteNodo( );
		}

		if( p.equals( ultimo ) )
		{
		ultimo = p.darAnteriorNodo( );
		}
		
		p.desconectarNodo( );
		numeroElementosLista--;
		}
	

	}

	@Override
	public Iterator<T> iterator() {
		return new IteradorParaListaDobleEnlazada();
	}

	class IteradorParaListaDobleEnlazada implements Iterator <T>{

		Nodo<T> nodoActual=null;
		@Override
		public boolean hasNext()
		{
			if (nodoActual==null && primero !=null)
			{
				return true;
			}
			else if(nodoActual!=null)
			{
				return nodoActual.darSiguienteNodo()!=null;
			}
			return false;
		}

		@Override
		public T next()
		{
			if (nodoActual==null)
			{
				nodoActual=primero;
			}

			else 
				{nodoActual= nodoActual.darSiguienteNodo();}

			return nodoActual.darItem();
		}
	}

}
