package model.data_structures;

public class Nodo<T> {
	
	/*
	 * Basada en mi ejercicio de nivel 9 de apo 2, el libro de introduccion a las estructuras de datos de Villalobos, y 
	 * de la implementacion de Jorge Villalobos para Cupi2  Recuperado de:
	 * https://cupintranet.virtual.uniandes.edu.co/sitio/index.php/cursos/estructuras-de-datos/cupi2collections
	 * /estructuras-de-datos/lista-encadenada/visualizacion-codigo-fuente/nodolista
	 */
	
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------



	private Nodo<T> siguienteNodo;
	private Nodo<T> anteriorNodo;
	private T item;
	
	// -----------------------------------------------------------------
	// Constructores
	// -----------------------------------------------------------------

	
	public Nodo (T pItem)
	{
		item= pItem;
		siguienteNodo=null;
		anteriorNodo=null;
	}
	
	// -----------------------------------------------------------------
	// Metodos
	// -----------------------------------------------------------------
	
	public T darItem()
	{
		return item;
	}
	
	public Nodo<T> darSiguienteNodo()
	{
		return siguienteNodo;
	}
	
	public Nodo<T> darAnteriorNodo()
	{
		return anteriorNodo;
	}
	
	public void cambiarAnteriorNodo(Nodo<T> pNodo)
	{
		anteriorNodo= pNodo;
	}
	
	public void cambiarSiguienteNodo(Nodo<T> pNodo)
	{
		siguienteNodo= pNodo;
	}
 	
	public void insertarNodoAntesDelActual(Nodo<T> pNodo)
	{
		pNodo.siguienteNodo=this;
		pNodo.anteriorNodo=anteriorNodo;
		if(anteriorNodo!=null)
		{
			anteriorNodo.siguienteNodo= pNodo;
			
		}
		anteriorNodo=pNodo;
	}
	
	public void insertarNodoDespuesDelActual(Nodo<T> pNodo)
	{
		pNodo.anteriorNodo=this;
		pNodo.siguienteNodo= this.siguienteNodo;
		if(siguienteNodo!=null)
		{
			siguienteNodo.anteriorNodo=pNodo;
		}
		siguienteNodo=pNodo;
	}
	
	public Nodo<T> desconectarPrimero( )
	{
	Nodo<T> p = siguienteNodo;
	siguienteNodo = null;
	if( p != null )
	{
	p.anteriorNodo = null;
	}
	return p;
	}
	
	public void desconectarNodo( )
	{
	Nodo<T> anterior = anteriorNodo;
	Nodo<T> siguiente = siguienteNodo;
	anteriorNodo = null;
	siguienteNodo = null;
	anterior.siguienteNodo = siguiente;
	if( siguiente != null )
	{
	siguiente.anteriorNodo = anterior;
	}
	}
}
