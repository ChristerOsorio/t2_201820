package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.DoublyLinkedListC;

public class DivvyTripsManager  implements IDivvyTripsManager {

	DoublyLinkedListC<VOTrip> listaViajes;
	DoublyLinkedListC<VOStation> listaEstaciones;


	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub

		FileReader reader;
		DoublyLinkedListC<VOStation> listaDobleEnlazada= new DoublyLinkedListC<VOStation>();

		try {
			reader= new FileReader(stationsFile);

			BufferedReader lector= new BufferedReader(reader);
			lector.readLine();
			String linea= lector.readLine();
			

			while(linea!=null)
			{
				
				
				String[] partes= linea.split(",");
				VOStation  viajeAAgregar;

			
				
				
					viajeAAgregar= new VOStation(partes[0], partes[1], partes[2], partes[3],  partes [4], partes [5]);
				
				





				listaDobleEnlazada.add(viajeAAgregar);
				linea= lector.readLine();


			}
			listaEstaciones=listaDobleEnlazada;


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}





	}


	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		FileReader reader;
		DoublyLinkedListC<VOTrip> listaDobleEnlazada= new DoublyLinkedListC<VOTrip>();

		try {
			reader= new FileReader(tripsFile);

			BufferedReader lector= new BufferedReader(reader);
			lector.readLine();
			String linea= lector.readLine();
			

			while(linea!=null)
			{
				
				
				String[] partes= linea.split(",");
				VOTrip  viajeAAgregar;

				int tamanioPartes= partes.length;
				if (tamanioPartes<11) 
				{
					viajeAAgregar= new VOTrip(partes[0], partes[4], partes[6], partes[8], "", partes [5], partes [7]);
				}
				else
				{
					viajeAAgregar= new VOTrip(partes[0], partes[4], partes[6], partes[8], partes[10], partes [5], partes [7]);
				}





				listaDobleEnlazada.add(viajeAAgregar);
				linea= lector.readLine();


			}
			listaViajes=listaDobleEnlazada;


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {

		DoublyLinkedListC<VOTrip> listaViajesGenero= new DoublyLinkedListC<VOTrip>();
		Iterator <VOTrip> iterador= listaViajes.iterator();
		while (iterador.hasNext())
		{ 
			VOTrip viaje= iterador.next();
			if (viaje.darGenero().equals(gender))
			{
				listaViajesGenero.add(viaje);
			}

		}
		return listaViajesGenero;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		DoublyLinkedListC<VOTrip> listaViajesAEstacionDestino= new DoublyLinkedListC<VOTrip>();
		Iterator <VOTrip> iterador= listaViajes.iterator();
		while (iterador.hasNext())
		{ 
			VOTrip viaje= iterador.next();
			if (viaje.darIdEstacionDestino()==stationID)
			{
				listaViajesAEstacionDestino.add(viaje);
			}

		}
		return listaViajesAEstacionDestino;
	}
}	



