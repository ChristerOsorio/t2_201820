package model.vo;

public class VOStation {
	
	private int id;
	private String nombre;
	private String ciudad;
	private double latitud;
	private double longitud;
	private int dpCapacity;
	
	public VOStation(String pId, String pNombre, String pCiudad, String pLatitud, String pLongitud, String pdpCapacity)
	{
		id= Integer.parseInt(pId);
		nombre=pNombre;
		ciudad=pCiudad;
		latitud=Double.parseDouble(pLatitud);
		longitud=Double.parseDouble(pLongitud);
		dpCapacity=Integer.parseInt(pdpCapacity);
	}


}
