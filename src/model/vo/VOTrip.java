package model.vo;

import java.io.File;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	//atributos 


	private int id;

	private double duracionViaje;

	private String nombreEstacionOrigen;

	private String nombreEstacionDestino;

	private String genero;

	private int idEstacionOrigen;

	private int idEstacionDestino;


	// constructor 

	public VOTrip(String pId, String pDuracionViaje, String pNombreEstacionOrigen, String pNombreEstacionDestino, String pGenero, String pIdEstacionOrigen, String pIdEstacionDestino)
	{

		id= Integer.parseInt(pId);
		duracionViaje= Integer.parseInt(pDuracionViaje);
		nombreEstacionOrigen=pNombreEstacionOrigen;
		nombreEstacionDestino=pNombreEstacionDestino;
		idEstacionOrigen= Integer.parseInt(pIdEstacionOrigen);
		idEstacionDestino= Integer.parseInt(pIdEstacionDestino);
		genero= pGenero;
	}

	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	


	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {

		return duracionViaje;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {

		return nombreEstacionOrigen;
	}

	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {

		return nombreEstacionDestino;
	}

	public String darGenero()
	{
		return genero;
	}

	public int darIdEstacionDestino()
	{
		return idEstacionDestino;
	}
}
